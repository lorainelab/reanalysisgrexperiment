# README #

This repository contains data analysis code for re-analysis of a microarray experiment that used the GR system to control activity of a transcriptional regulator (Rga) involved in floral development. 

### Reproducing the analysis ###

Clone the repository and then open the R project (.Rproj) file in RStudio. Then open and run .Rmd files.

### Questions? ###

Contact Ann Loraine (aloraine@uncc.edu) or Beth Krizek (bakrizek@mailbox.sc.edu).